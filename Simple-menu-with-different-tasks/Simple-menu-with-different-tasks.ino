#include <Zumo32U4.h>  //

Zumo32U4LCD lcd;          // Display (8x2)
Zumo32U4ButtonA buttonA;  // Taster A
Zumo32U4ButtonB buttonB;  // Taster B
Zumo32U4ButtonC buttonC;  // Taster C
Zumo32U4Motors motors;    // Motoren

int leftSpeed = 200;
int rightSpeed = 200;

void setup() {
  menu();
}

void menu() {
  ledGreen(0);
  ledYellow(0);
  ledRed(0);
  lcd.clear();
  lcd.print("A  B  C");
}

void loop() {
  if (buttonA.isPressed()) {
    ledYellow(1);
    lcd.clear();
    lcd.print("straight");
    delay(500);
    motors.setSpeeds(leftSpeed, rightSpeed);
    delay(2000);
    motors.setSpeeds(0, 0);
    
    menu();
    // - turn on yellow LED, diplay shows "straight"
    // - waits 0.5 seconds
    // - drive 50 cm straight
    // - turn off yellow LED, show menu "A B C"
  } else if (buttonB.isPressed()) {
    ledGreen(1);
    lcd.clear();
    lcd.print("accelerate");
    delay(500);
    for (int i = 0; i < 400; i++) {
      motors.setSpeeds(i, i);
      delay(5);
    }
    for (int i = 400; i > 0; i--) {
      motors.setSpeeds(i, i);
      delay(5);
    }

    menu();
    // - turn on green LED, display shows "accelerate"
    // - wait 0.5 seconds
    // - accelerate the robot slowly from 0 to maximum speed (approx. 1 second)
    // - afterwards decelarte the the speed slowly back to zero
    // - turn off green LED, show menu "A  B  C"
  } else if (buttonC.isPressed()) {

    ledRed(1);
    lcd.clear();
    lcd.print("rectangle");
    delay(500);

    for (int i = 0; i < 4; i++){
      motors.setSpeeds(200, 200);
      delay(2000);
      motors.setSpeeds(-150, 150);
      delay(690);
    }
    motors.setSpeeds(0,0);

    menu();
    // - turn on red LED , display shows "rectangle"
    // - wait 0.5 seconds
    // - drive the robot a rectangle of 20cm x 20cm
    // - start and end point should be indentical
    // - turn off red LED, show menu "A  B  C"
  }
}