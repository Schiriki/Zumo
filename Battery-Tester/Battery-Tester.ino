#include <Zumo32U4.h>

Zumo32U4LCD lcd;
Zumo32U4ButtonA buttonA;



void setup() {
  testBattery();
}

void loop() {
  // put your main code here, to run repeatedly:
}

void testBattery() {
  long lastCheck = 0;
  while (buttonA.isPressed() == 0) {
    unsigned long time = millis();
    if (time - lastCheck >= 1000) {
      int voltage = readBatteryMillivolts();
      lcd.clear();

      if (voltage < 4200) {
        lcd.print(voltage);
        lcd.print("  mV");
        lcd.gotoXY(0, 1);
        lcd.print("BAT: LOW");
        ledRed(1);
        ledGreen(0);
      }else if (buttonA.isPressed()) {

      } else {
        lcd.print(voltage);
        lcd.print("  mV");
        lcd.gotoXY(0, 1);
        lcd.print("BAT:  OK");
        ledRed(0);
        ledGreen(1);
      }
      lastCheck = time;
    }
  }
  ledRed(0);
  ledGreen(0);
  lcd.clear();
}
