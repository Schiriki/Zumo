#include <Zumo32U4.h>  // Einbinden der ZUMO Bibliothek

Zumo32U4Motors motors;
Zumo32U4ButtonA buttonA;
Zumo32U4ButtonB buttonB;
Zumo32U4ButtonC buttonC;
Zumo32U4LCD lcd;
Zumo32U4Encoders encoders;

int distance = 600; //Distance in mm
int mSpeed = 350;
float kp = 1.2;
float kd = 0;

void setup() {
  testBattery();
  Serial.begin(9600);
  //lcd.print(testError());
}

void loop() {
  lcd.print("[A] ->");
  buttonA.waitForButton();
  lcd.clear();
  delay(700);
  driveDistance(distance, mSpeed, 1); //TODO: upadte values

}

  void driveDistance(int pDistanceInMM, int pSpeed, float pStraigthFaktor) {
  long distanceInTicks = mmToTicks(pDistanceInMM);
  long averageEncoderTicks = 0;
  encoders.getCountsAndResetLeft();
  encoders.getCountsAndResetRight();
  long ticksLeft;
  long ticksRight;
  int error = 0;
  int differential = 0;
  int dSpeed = 0;
  int error_old = 0;
  int MSPEED = pSpeed;
  while (averageEncoderTicks <= distanceInTicks) {
    ticksLeft = encoders.getCountsLeft();
    ticksRight = encoders.getCountsRight();
    averageEncoderTicks = (ticksLeft + ticksRight) / 2;
    error = ticksLeft - pStraigthFaktor * ticksRight;
    differential = error - error_old;
    error_old = error;
    dSpeed = (error * kp) + (differential * kd);
    if (averageEncoderTicks > (distanceInTicks - 95)) {
      motors.setSpeeds(10, 10);
    }

    motors.setSpeeds(MSPEED - dSpeed, MSPEED + dSpeed);
    Serial.println(averageEncoderTicks);
  }
  motors.setSpeeds(0, 0);
}

int mmToTicks (int pMm) {
  return pMm / 0.13;
}

int testError() {
  encoders.getCountsAndResetLeft();
  encoders.getCountsAndResetRight();
  lcd.gotoXY(0, 0);
  lcd.print("Test");
  lcd.gotoXY(0, 1);
  lcd.print("Error");
  motors.setSpeeds(200, 200);
  int distanceInTicks = mmToTicks(1000);
  int averageEncoderTicks = 0;
  while (averageEncoderTicks <= distanceInTicks) {
    averageEncoderTicks = (encoders.getCountsLeft() + encoders.getCountsRight()) / 2;
  }
  motors.setSpeeds(0, 0);
  lcd.clear();
  return (encoders.getCountsLeft() - encoders.getCountsRight());
}
