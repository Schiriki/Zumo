#include <Zumo32U4.h>

Zumo32U4ButtonA buttonA;
Zumo32U4ButtonB buttonB;
Zumo32U4ButtonC buttonC;

void setup() {
  // put your setup code here, to run once:

}

void loop() {
  if (buttonA.isPressed() == true) {
    ledYellow(1);
  }
  else {
    ledYellow(0);
  }
  if (buttonC.isPressed() == true) {
    ledRed(1);
  }
  else {
    ledRed(0);
  }
  if (buttonB.isPressed() == true) {
    ledGreen(1);
  }
  else {
    ledGreen(0);
  }
}