void myCalibrate() {
  lcd.clear();
  lcd.print("Calibr..");
  delay(500);
  for (int i = 0; i < 120; i++) {
    if (i > 30 && i <= 90) {
      motors.setSpeeds(-200, 200);
    } else {
      motors.setSpeeds(200, -200);
    }
    lineSensors.calibrate();  // Sensoren auswerten und kalibrieren
  }
  motors.setSpeeds(0, 0);  // Motor stoppen
  lcd.clear();
}
