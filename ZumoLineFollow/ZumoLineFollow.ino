#include <Zumo32U4.h>  // Einbinden der ZUMO Bibliothek

Zumo32U4LCD lcd;  // Objekt für das LCD-Display
Zumo32U4ButtonA buttonA;
Zumo32U4ButtonB buttonB;
Zumo32U4ButtonC buttonC;
Zumo32U4LineSensors lineSensors;
Zumo32U4Motors motors;

#define NUM_SENSORS 5                        // number of used sensors
unsigned int lineSensorValues[NUM_SENSORS];  // array for sensor values

unsigned int timeStamp = 0;

int state = 1; //Used for switch in the loop
float kp = 1;
float kd = 3;
int mSpeed = 350;

void setup() {
  testBattery();
  Serial.begin(115200);

  lineSensors.initFiveSensors();  // initialize the line sensors

  lcd.clear();
  lcd.print("St. Cali");
  lcd.gotoXY(0, 1);
  lcd.print("-> [A]");
  buttonA.waitForPress();
  myCalibrate();
  lcd.clear();
  lcd.print("Run [A]");
  buttonA.waitForPress();
  delay(500);
  lcd.clear();
  lcd.print("Menu [A]");
}

void followEdge(float pKp, float pKd, int pSpeed) {
  lineSensors.readCalibrated(lineSensorValues);  // Sensorwerte einlesen
  int sensorValue = lineSensorValues[2];         // Index 2 : Mittensensor
  static int eOld = 500;
  int e = 500 - sensorValue;
  int diff = e - eOld;
  int deltaSpeed = (pKp * e) + (pKd * diff);
  int leftSpeed = pSpeed + deltaSpeed;
  int rightSpeed = pSpeed - deltaSpeed;
  eOld = e;
  motors.setSpeeds(leftSpeed, rightSpeed);
}

void menu1() {
  if ((millis() - timeStamp) > 400 ) {
    timeStamp = millis();
    lcd.clear();
    lcd.print("b>+ c>-");
    lcd.gotoXY(0, 1);
    lcd.print("Kp = ");
    lcd.print(kp);
  }
  if (buttonB.getSingleDebouncedPress()) {
    kp = kp + 0.1;
  }
  if (buttonC.getSingleDebouncedPress()) {
    kp = kp - 0.1;
  }
  if (buttonA.getSingleDebouncedPress()) {
    state = 3;
  }
}

void menu2() {
  if ((millis() - timeStamp) > 400 ) {
    timeStamp = millis();
    lcd.clear();
    lcd.print("b>+ c>-");
    lcd.gotoXY(0, 1);
    lcd.print("Kd = ");
    lcd.print(kd);
  }
  if (buttonB.getSingleDebouncedPress()) {
    kd = kd + 1;
  }
  if (buttonC.getSingleDebouncedPress()) {
    kd = kd - 1;
  }
  if (buttonA.getSingleDebouncedPress()) {
    state = 4;
  }
}

void menu3() {
  if ((millis() - timeStamp) > 400 ) {
    timeStamp = millis();
    lcd.clear();
    lcd.print("b>+ c>-");
    lcd.gotoXY(0, 1);
    lcd.print("Sped=");
    lcd.print(mSpeed);
  }
  if (buttonB.getSingleDebouncedPress()) {
    mSpeed = mSpeed + 10;
  }
  if (buttonC.getSingleDebouncedPress()) {
    mSpeed = mSpeed - 10;
  }
  if (buttonA.getSingleDebouncedPress()) {
    lcd.clear();
    lcd.print("Run [A]");
    state = 1;
  }
}

void loop() {
  switch (state) {
    case 1:
      followEdge(kp, kd, mSpeed);
      if (buttonA.getSingleDebouncedPress()) {
        state = 2;
      }
      break;
    case 2:
      motors.setSpeeds(0, 0);
      menu1();
      break;
    case 3:
      menu2();
      break;
    case 4:
      menu3();
      break;
  }
}

/*void showSensorValue() {
  if ((millis() - timeStamp) > 500) {
    lcd.clear();
    timeStamp = millis();
    lineSensors.read(lineSensorValues);      // read all sensors
    int sensorCenter = lineSensorValues[2];  // get value of center sensor (index 2)
    int sensorLeft = lineSensorValues[0];
    int sensorRight = lineSensorValues[4];
    lcd.gotoXY(0, 0);
    lcd.print(sensorCenter);
    lcd.gotoXY(4, 0);
    lcd.print(sensorLeft);
    lcd.gotoXY(0, 1);
    lcd.print(sensorRight);
  }
  }*/
