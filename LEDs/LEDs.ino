#include <Zumo32U4.h>

unsigned long zeitLetztesUmschalten = 0;
unsigned long timer = 1000;
bool ledState = 0;

void setup() {
  // put your setup code here, to run once:

}

void loop() {
  unsigned long currentTime = millis();

  if ((currentTime - zeitLetztesUmschalten) >= timer){
     if (ledState == 0) {
       ledState = 1;
     }
     else {
       ledState = 0;
     }
    ledGreen(ledState);
    zeitLetztesUmschalten = currentTime;
  }
}
