#include <Zumo32U4.h>

Zumo32U4ButtonA buttonA;
Zumo32U4Motors motors;

void setup() {
  ledGreen(1);
}

void loop() {
  buttonA.waitForButton();
  ledGreen(0);
  motors.setSpeeds(100, 200); 
  delay(7500);
  motors.setSpeeds(0, 0);
  ledGreen(1);
}
