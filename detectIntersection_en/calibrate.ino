// Kalibrierung der Bodensensoren durch automatisches drehen des Roboters über der Linie.
void calibrateSensors() {
  lcd.clear();
  lcd.print(F("Calib ..."));
  delay(500);

  // Kalibirierungsschleife: Roboter dreht sich und nimmt 120 Messwerte auf
  for (int i = 0; i < 120; i++) {
    if (i > 30 && i <= 90) {
      motors.setSpeeds(-200, 200);
    } else {
      motors.setSpeeds(200, -200);
    }
    sensors.calibrate();  // Sensoren auswerten und kalibrieren
  }
  motors.setSpeeds(0, 0);     // Motor stoppen
  lcd.clear();
}
