// p-controller to follow the edge of a line
void followLine(int pPos) {
  static int e_old; // will never be deleted
  int e = pPos - LINE;
  int dSpeed = kp * e + kd * (e - e_old);
  motors.setSpeeds(mspeed + dSpeed, mspeed - dSpeed);
  e_old = e;
}
