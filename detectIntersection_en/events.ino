void reagierenAufEvent(int pEventNummer) {
  motors.setSpeeds(0, 0);
  delay(1000);

  switch (pEventNummer) {
    case 1:
    case 2:
    case 5:
      lcd.clear();
      lcd.print("Event");
      turn(1);
      break;
  }
}

void turn(int pRichtung) {
  // 1. Der Roboter fährt mittig auf die Kreuzung
  motors.setSpeeds(100, 100);
  delay(1000);
  // 2. Der Roboter dreht sich nach links/rechts
  if (pRichtung = 0) {
    motors.setSpeeds(-100, 100);
    delay(600);

  }
  else if (pRichtung = 1) {
    motors.setSpeeds(100, -100);
    delay(600);
  }
  // 3. Das Drehen wird gestoppt, wenn der Roboter
  //    mittig auf dem neuen Weg steht
}
