// PD - Controller to follow the edge of a line
// no menue

#include <Zumo32U4.h>  // Einbinden der ZUMO Bibliothek

Zumo32U4LCD lcd;  // LCD object
Zumo32U4ButtonA buttonA;
Zumo32U4ButtonB buttonB;
Zumo32U4ButtonC buttonC;
Zumo32U4LineSensors sensors;
Zumo32U4Motors motors;

#define NUM_SEN 5
unsigned int sensorValues[NUM_SEN];

const int LINE = 2000;    // desired value for the controller
int mspeed = 200;        // average motor speed
float kp = 1.2;          // default proportional gain value
float kd = 3;

int eventNummer = 0;

void setup() {
  Serial.begin(115200);
  testBattery();
  sensors.initFiveSensors();
  lcd.gotoXY(0, 1);
  lcd.print("Cali A");
  buttonA.waitForPress();
  calibrateSensors();

  lcd.gotoXY(0, 1);
  lcd.print("Start A");
  buttonA.waitForPress();
  delay(500);
}

void loop() {
  int position = sensors.readLine(sensorValues);
  if (!istEvent()) {                           // keine Kreuzung, also Linie
    followLine(position);
  }
  else {
    eventNummer++;                       // Zählvariable für die Events
    if (eventNummer < 12) {
      reagierenAufEvent(eventNummer);    // Abhängig vom Event abbiegen, weiterfahren, etc.
    }
    else  {                              // letztes  Event ist erreicht
      motors.setSpeeds(0, 0);
      lcd.print(" -ENDE- ");
      buttonA.waitForButton();
      eventNummer = 0;
    }
  }
}



bool istEvent() {
  // is any junction left or right (includes the dessert)
  // is the robot above a line

  bool junctionLeft  = sensorValues[0] > 300; // outer left sensor
  bool junctionRight = sensorValues[4] > 300; // outer right sensor
  bool noLine        = sensorValues[2] < 600; // middle sensor

  // (junctionLeft OR junctionRigth OR noLine)
  if (junctionLeft || junctionRight || noLine) {
    return true;
  }
  else {
    return  false;
  }
  // return sensorValues[0] > 300 || sensorValues[4] > 300 || sensorValues[2] < 600;
}
