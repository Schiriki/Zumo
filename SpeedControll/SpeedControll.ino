#include <Zumo32U4.h>
Zumo32U4Motors motors;
Zumo32U4Encoders encoders;
Zumo32U4ButtonA buttonA;
Zumo32U4LCD lcd;

unsigned long dZeit    =   5 * 1e3;        // Zeitintervall für die Geschwindigkeitsmessung (5000 Mikrosekunden)
unsigned long fahrzeit = 501 * 1e3;        // Dauer der Messung (0,5 Sekunden)

unsigned long jetzt         = micros();
unsigned long startzeit     = jetzt;       // Startzeitpunkt der Messreihe
unsigned long letzteMessung = jetzt;       // Zeitpunkt der letzten durchgeführten Messung


void setup() {
  Serial.begin(115200);
  testBattery();
  drive(300);

}

void loop() {
  // put your main code here, to run repeatedly:

}

void drive(int pSpeed) {
  encoders.getCountsAndResetRight();
  encoders.getCountsAndResetLeft();
  
  motors.setSpeeds(pSpeed, pSpeed);
  jetzt = micros();
  startzeit = jetzt;
  while (jetzt - startzeit < fahrzeit) {
    jetzt = micros();
    if (jetzt - letzteMessung >= dZeit) {
      letzteMessung = jetzt;
      int impulseRight = encoders.getCountsAndResetRight(); // Impulse auslesen und zurücksetzen
      int impulseLinks = encoders.getCountsAndResetLeft();
      int vRobotLinks = (impulseLinks * 1e6 ) / dZeit;
      int vRobotRight = (impulseRight * 1e6 ) / dZeit;
      Serial.print((jetzt - startzeit) / 1000);
      Serial.print("\t");
      Serial.print(vRobotLinks);
      Serial.print("\t");
      Serial.println(vRobotRight);
    }
  }
  motors.setSpeeds(0, 0);
}
